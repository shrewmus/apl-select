const fs = require('fs-extra');
const concat = require('concat');

(async function build() {
  const files =[
    './dist/apl-select-dropdown/runtime-es2015.js',
    './dist/apl-select-dropdown/polyfills-es2015.js',
    './dist/apl-select-dropdown/scripts.js',
    './dist/apl-select-dropdown/main-es2015.js'
  ];

  await fs.ensureDir('elements');
  await concat(files, 'elements/apl-select.js');
  await fs.copyFile('./dist/apl-select-dropdown/styles.css', 'elements/styles.css');
  // await fs.copy('./dist/')
})();
