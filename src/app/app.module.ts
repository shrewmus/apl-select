import { BrowserModule } from '@angular/platform-browser';
import {Injector, NgModule} from '@angular/core';

import { AplSelectComponent } from './apl-select/apl-select.component';
import {createCustomElement} from '@angular/elements';

@NgModule({
  declarations: [
    AplSelectComponent
  ],
  imports: [
    BrowserModule
  ],
  entryComponents: [AplSelectComponent]
})
export class AppModule {

  constructor(private injector: Injector) {
    const aplSelect = createCustomElement(AplSelectComponent, {injector});
    customElements.define('apl-select', aplSelect);
  }

  ngDoBootstrap() {

  }
}
