import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AplSelectComponent } from './apl-select.component';

describe('AplSelectComponent', () => {
  let component: AplSelectComponent;
  let fixture: ComponentFixture<AplSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AplSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AplSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
