import {ChangeDetectorRef, Component, Input, OnInit} from '@angular/core';


export interface ListItem {
  name: string;
  id: any;
  child: ListItem[];
}

@Component({
  selector: 'apl-select',
  templateUrl: './apl-select.component.html',
  styleUrls: ['./apl-select.component.scss'],
})
export class AplSelectComponent implements OnInit {
  private _items: ListItem[] = [];
  private _selected: ListItem[] = [];
  private _isMultiple: any;
  private opennedChilds: any = {};
  private checked: any = {};

  isOpen: boolean = false;

  @Input('emptySelectText')
  emptySelectText: string = 'Please select departments';

  @Input('selected')
  set selected(val: ListItem[]) {

    if (!Array.isArray(val)) {
      return;
    }

    this.checked = {};
    if (this._isMultiple) {
      this._selected = val;
      this.checked = {}
    } else {
      this._selected = [val[0]];
    }

    this._selected.forEach(item => { this.checked[item.id] = 1 });
    this.changeRef.detectChanges();
  }

  get selected(): ListItem[] {
    return this._selected;
  }

  @Input('multiple')
  set multiple(val: any) {
    this._isMultiple = val;
  }

  get multiple() {
    return this._isMultiple;
  }

  @Input('items')
  set items(val: any) {

    if (typeof val === 'string') {
      this._items = JSON.parse(val);
    } else {
      this._items = val;
    }

    this.changeRef.detectChanges();
  }

  get items(): any {
    return this._items;
  }

  constructor(private changeRef: ChangeDetectorRef) {
  }

  ngOnInit() {
  }

  onItemClick(item: any) {
    if (this.checked[item.id] !== undefined) {
      delete this.checked[item.id];
      this._selected = this._selected.filter(selItem => selItem.id !== item.id);
    } else {
      if (this._isMultiple) {
        this.checked[item.id] = 1;
        this._selected.push(item);
      } else {
        this.checked = {};
        this.checked[item.id] = 1;
        this._selected = [item];
      }
    }
    this.changeRef.detectChanges();
  }

  isChildOpen(item: any) {
    return (this.opennedChilds[item.id] !== undefined) ? this.opennedChilds[item.id] : false;
  }

  toggleChild(item: any) {
    if (this.opennedChilds[item.id] === undefined) {
      this.opennedChilds[item.id] = true;
    } else {
      this.opennedChilds[item.id] = !this.opennedChilds[item.id];
    }
    this.changeRef.detectChanges();
  }

  isItemChecked(item: any) {
    return this.checked[item.id] !== undefined;
  }

  toggleSelect() {
    this.isOpen = !this.isOpen;
    this.changeRef.detectChanges();
  }
}

